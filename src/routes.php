<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');


$app->get('/users', function (Request $request, Response $response, $args) {
    $url = 'https://akela.mendelu.cz/~xvalovic/mock_users.json';
    $users = file_get_contents($url);
    $data['users'] = json_decode($users, true);
    /*
    $data = [
        'users' => [
            {
                'id': 123,
                'name': 'aaa',
            }
        ]
    ]
    */
    return $this->view->render($response, 'users.latte', $data);
})->setName('users_list');


$app->get('/login', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'login.latte');
})->setName('login_form');

$app->post('/login', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    //$formData = ['username' => 'Johny', 'passwd' => 'heslo123']

    return $this->view->render($response, 'login.latte', $formData);
});

